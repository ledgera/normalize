# `normalize` package

`go get gitlab.com/ledgera/normalize`

This package holds Go code for common methods to scale/normalize a range of data values.
